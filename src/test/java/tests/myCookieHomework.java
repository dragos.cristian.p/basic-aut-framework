package tests;

import Utils.OtherUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class myCookieHomework extends BaseTest {
    @Test
    public void cookieCheckTest() {
        driver.manage().deleteAllCookies();
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(driver);
        System.out.println("Creating the cookie");
        Cookie myNewCookie = new Cookie("myFirstCookie", "12345678910");
        driver.manage().addCookie(myNewCookie);
        OtherUtils.printCookies(driver);
        OtherUtils.printCookie(myNewCookie);
        Assert.assertEquals(myNewCookie.getValue(), "12345678910");
        System.out.println("Deleting the cookie");
        driver.manage().deleteCookie(myNewCookie);
        WebElement removeTheCookie = driver.findElement(By.id("delete-cookie"));
        removeTheCookie.click();
        OtherUtils.printCookies(driver);
        String newCookieValue = driver.findElement(By.id("cookie-value")).getText();
        Assert.assertEquals(newCookieValue, "");

    }

}

